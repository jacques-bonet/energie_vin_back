import { MigrationInterface, QueryRunner } from "typeorm";

export class Seeds1800752758454 implements MigrationInterface {
    name = 'Seeds1800752758454'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`INSERT INTO bottle VALUES (nextval('bottle_id_seq'), 0, 'Plonk', 4, 1, 1, 2021, CURRENT_DATE, CURRENT_DATE, 2)`);
        await queryRunner.query(`INSERT INTO bottle VALUES (nextval('bottle_id_seq'), 0, 'The good stuff', 100, 1, 1, 1990, CURRENT_DATE, CURRENT_DATE, 10)`);
        await queryRunner.query(`INSERT INTO bottle VALUES (nextval('bottle_id_seq'), 0, 'Monkey milk', 20, 1, 1, 2019, CURRENT_DATE, CURRENT_DATE, 3)`);
        await queryRunner.query(`INSERT INTO bottle VALUES (nextval('bottle_id_seq'), 0, 'Savergnet Caubinon', 15, 1, 1, 2013, CURRENT_DATE, CURRENT_DATE, 5.5)`);
        await queryRunner.query(`INSERT INTO bottle VALUES (nextval('bottle_id_seq'), 0, 'Battery Acid', 1, 1, 1, 2023, CURRENT_DATE, CURRENT_DATE, 1)`);

        await queryRunner.query(`INSERT INTO bottle_price_history VALUES (nextval('bottle_price_history_id_seq'), 1, 3, CURRENT_DATE - interval '1 year');`);
        await queryRunner.query(`INSERT INTO bottle_price_history VALUES (nextval('bottle_price_history_id_seq'), 1, 5, CURRENT_DATE - interval '2 years');`);
        await queryRunner.query(`INSERT INTO bottle_price_history VALUES (nextval('bottle_price_history_id_seq'), 1, 4, CURRENT_DATE - interval '3 years');`);
        await queryRunner.query(`INSERT INTO bottle_price_history VALUES (nextval('bottle_price_history_id_seq'), 2, 90, CURRENT_DATE - interval '3 years');`);

        await queryRunner.query(`INSERT INTO note VALUES (nextval('note_id_seq'), 1, 1, 1, CURRENT_DATE, CURRENT_DATE, 'beurk!');`);
        await queryRunner.query(`INSERT INTO note VALUES (nextval('note_id_seq'), 1, 1, 0, CURRENT_DATE, CURRENT_DATE, 'it burns!');`);
        await queryRunner.query(`INSERT INTO note VALUES (nextval('note_id_seq'), 2, 1, 9, CURRENT_DATE, CURRENT_DATE, 'yum!');`);
        await queryRunner.query(`INSERT INTO note VALUES (nextval('note_id_seq'), 2, 1, 10, CURRENT_DATE, CURRENT_DATE - interval '1 year', 'omfg!');`);
        await queryRunner.query(`INSERT INTO note VALUES (nextval('note_id_seq'), 2, 1, 11, CURRENT_DATE, CURRENT_DATE - interval '2 year', 'freakin delicious!');`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "note" DROP CONSTRAINT "FK_50f7a840413b64611b96d8c6f1b"`);
    }
}
