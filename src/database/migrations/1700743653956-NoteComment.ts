import { MigrationInterface, QueryRunner } from "typeorm";

export class NoteComment1700743653956 implements MigrationInterface {
    name = 'NoteComment1700743653956'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "note" ADD "comment" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "note" DROP COLUMN "comment"`);
    }
}
