import { ApiProperty, PartialType } from '@nestjs/swagger';

import { CreateBottleDto } from './create-bottle.dto';

export class UpdateBottleDto extends PartialType(CreateBottleDto) {
  @ApiProperty()
  id: number;
}
