import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { NoteEntity } from 'src/notes/entities';
import { TimedSqlEntity } from 'src/shared/models/entities';

@Entity('bottle')
export class BottleEntity extends TimedSqlEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 100 })
  name: string;

  @Column({ type: 'numeric' })
  price: number;

  @Column({ name: 'producer_id' })
  producerId: number;

  @Column({ name: 'retailer_id' })
  retailerId: number;

  @Column()
  type: number;

  @Column()
  year: number;

  @OneToMany(() => NoteEntity, (note) => note.bottleId, { /*cascade: true,*/ eager: true })
  notes: NoteEntity[];

  @Column({ type: 'numeric' })
  note: number;
}
